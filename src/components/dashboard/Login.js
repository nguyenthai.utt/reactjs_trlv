import React from 'react'
import { useFormik } from 'formik';

function Login() {
    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
        },
        onSubmit: values => {
            console.log(values);
            alert(JSON.stringify(values, null, 2));
        },
    });

    return (
        <div className='hero-container' id="login">
            <video src="/videos/login.mp4" autoPlay loop muted />
            <h1>ADVENTURE AWAITS</h1>
            <p>What are you waiting for?</p>
            <div className="container">
                <form onSubmit={formik.handleSubmit} className="login-form">
                    <div className="form-group">
                        <label for="exampleInputEmail1">User Name</label>
                        <input
                            name="username"
                            className="form-control"
                            id="exampleInputEmail1"
                            placeholder="User Name"
                            onChange={formik.handleChange}
                            value={formik.values.username}
                        />
                    </div>
                    <div className="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input
                            name="password"
                            type="password"
                            className="form-control"
                            id="exampleInputPassword1"
                            placeholder="Password"
                            onChange={formik.handleChange}
                            value={formik.values.password}
                        />
                    </div>
                    <div className="form-group form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                        <label className="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <div className="form-group form-button">
                        <button type="submit" className="btn btn-primary">Login</button>
                    </div>

                </form>
            </div >
        </div>

    )
}

export default Login
