import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import './css/Dashboard.css'

function RouterDoashboard(props) {
    const history = useHistory();
    useEffect(() => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }, [history.location.pathname])

    const isLogin = localStorage.getItem('login');

    let check = false;
    props.children?.forEach(item => {
        if (props.location.pathname === item?.props.path) { check = true }
    })

    useEffect(() => {
        if (!isLogin && check) history.push('/login')
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLogin])



    return (
        <div>
            {props.children}
        </div>
    )
}

export default RouterDoashboard
